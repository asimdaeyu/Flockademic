import { GetPeriodicalsResponse } from '../../../../lib/interfaces/endpoints/periodical';
import { Request } from '../../../../lib/lambda/faas';
import { DbContext } from '../../../../lib/lambda/middleware/withDatabase';
import { fetchPeriodicals as commandHandler } from '../services/fetchPeriodicals';

export async function fetchPeriodicals(
  context: Request<undefined> & DbContext,
): Promise<GetPeriodicalsResponse> {
  try {
    const result = await commandHandler(context.database);

    return result;
  } catch (e) {
    throw new Error('Could not fetch journals.');
  }
}
