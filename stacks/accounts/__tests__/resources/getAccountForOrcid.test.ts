jest.mock('../../src/dao', () => ({
  getAccountForOrcid: jest.fn().mockReturnValue(Promise.resolve({ identifier: 'arbitrary-id' })),
}));

import { getAccountIdForOrcid } from '../../src/resources/getAccountIdForOrcid';

const mockContext = {
  body: {},
  database: {} as any,

  headers: {},
  method: 'GET' as 'GET',
  params: [ '/ids/0000-0002-4013-9889', '0000-0002-4013-9889' ],
  path: '/ids/0000-0002-4013-9889',
  query: null,
};

it('should return the fetched account ID', () => {
  const mockedDao = require.requireMock('../../src/dao');
  mockedDao.getAccountForOrcid.mockReturnValueOnce(Promise.resolve(
    { identifier: 'some-id' },
 ));

  return expect(getAccountIdForOrcid(mockContext)).resolves.toEqual({
    identifier: 'some-id',
  });
});

it('should error when no account identifiers were passed', () => {
  return expect(getAccountIdForOrcid({
    ...mockContext,
    params: [ '/ids/' ],
    path: '/ids/',
  })).rejects.toEqual(new Error('Cannot fetch account ID without an ORCID.'));
});

it('should error when the account ID does not exist', () => {
  const mockedDao = require.requireMock('../../src/dao');
  mockedDao.getAccountForOrcid.mockReturnValueOnce(Promise.resolve(null));

  return expect(getAccountIdForOrcid(mockContext)).rejects.toEqual(new Error('Could not fetch account ID.'));
});

it('should error when the account ID could not be fetched', () => {
  const mockedDao = require.requireMock('../../src/dao');
  mockedDao.getAccountForOrcid.mockReturnValueOnce(Promise.reject('Arbitrary error'));

  return expect(getAccountIdForOrcid(mockContext)).rejects.toEqual(new Error('Could not fetch account ID.'));
});
